# Задача

1. Развернуть у себя minikube. (Если есть свой кластер в облаке – можно на нем)
2. Развернуть Jenkins, не важно где, главное чтобы с него был доступ до куба, и он мог собирать образы.
3. Взять любое примитивное Java Spring Boot приложение (Например https://github.com/goxr3plus/Simplest-Spring-Boot-Hello-World ) и подключить к нему spring-boot-actuator (https://habr.com/ru/company/otus/blog/452624/ )
4. Сделать pipe build и deploy в Jenkins.
   a. Собрать образ и выложить его в любой regestry, например на Dockerhub.
   b. Подготовить манифесты для деплоя в kubernetes, в manifest должны быть пробы на endpoints spring-boot-actuator. Также нужно выставить endpoint наружу, чтобы мы могли обратиться к нему у вас в браузере.
   Проверка задания будет выполняться демонстрацией работы pipe с вашего экрана.