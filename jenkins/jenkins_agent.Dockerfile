# запустить команду для сборки образа агента
# DOCKER_BUILDKIT=0 docker build -t jnkns-agent-mvn -f jenkins_agent.Dockerfile .
FROM jenkins/ssh-agent:jdk11
RUN id && apt update && apt install -y maven git
RUN apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
RUN mkdir -p /etc/apt/keyrings && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN chmod a+r /etc/apt/keyrings/docker.gpg
RUN lsb_release -cs
RUN apt update && apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
#RUN groupadd -g 1001 docker
RUN usermod -aG docker jenkins
