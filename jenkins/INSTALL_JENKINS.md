* ### Установить Jenkins в Docker
initial password: bd4e3a57adfd4db1a8e3e5f3ce3d7286

1. Выполнить:
    ```bash
    docker pull jenkins/jenkins:lts
    docker run --network jenkins_network -d -p 8080:8080 -p 50000:50000 -v ~/jenkinstemp:/var/jenkins_home jenkins/jenkins:lts
    ```

2. Далее следовать встроенной инструкции

* ### Создать и привязать агент согласно инструкции https://www.jenkins.io/doc/book/using/using-agents/

1. Запустить (Для Windows использовать образ `jenkins/ssh-agent:jdk11` как базовый)
    `docker run -d --rm --name=agent1 --network jenkins_network -v /var/run/docker.sock:/var/run/docker.sock -p 22:22 -e "JENKINS_AGENT_SSH_PUBKEY=ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCu8QUNvgzYAypROVjRS4rvhUFxleEDtjrPDiUT5566rD3yFSCOXiB/AoSKE2LTG/AKDuooyHZH925BFm1NrNIkDJC/M24tGGYfPi755yOc4jz+/DQUuTvmP0z/VPebrfiV4hjenuucDL73hGMQCuQNSEIr8VJTtDyRomS3wIVtsjYQah1n0+fGB/bHupFdB94vIlEW45poNzWotSldzEY0FKUMCvdM6Lqddwwpe1cPy+x/xftArBPTaQYDMkBQY6TJ3ZsYxKe/9dF9IDWCGSSa8hrAUCHGpkjEf6QBBxHPjTpUBbeHh5qNm9nbA3b0dInoO4v7PdW/KCt21XrKhOpY8PVdixhlyAq2PCa74h8dE/PMQS2oqzSYJtJYEc9OFWeKettjI3hKd/rxDXPWalevQCIRG6bc4YAIyCCJdu4jRikrACy/WjeQvrnSWJX4yCvBvfF6g3PNnwsS2z9PeEPpxUdgOHaa/qfQicwKrmSrcusR1VEzK5cMcZ9UaEt79yM= nikki@Nikki" jnkns-agent-mvn`

2. Установить maven на агент 
    ```bash
    apt update
    apt install maven
    ```
3. Залогиниться в докер репозиторий на агенте (использую docker hub) ```docker login -u [LOGIN] -p [PASSWORD]```


